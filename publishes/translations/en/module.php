<?php

return [
    'backend'     => 'Administration panel',
    'success'     => 'Success!',
    'error'       => 'Error!',
    'settings'    => 'Settings',
    'actions'     => 'Actions',
    'login'       => 'Sign In',
    'dashboard'   => 'Dashboard',
    'credentials' => [
        'email'    => 'Email',
        'username' => 'Username',
        'password' => 'Password',
    ],
    'resources'   => [
        'users'     => 'Members',
        'languages' => 'Languages',
        'settings'  => 'Settings',
    ],
    'groups'      => [
        'users' => 'User Groups',
    ],
    'action' => [
        'create' => 'Create :resource',
        'edit'   => ':instance',
        'view'   => ':instance',
    ]
];
