@inject('template', 'scaffold.template')

@inject('blankPanel', 'App\Http\Terranet\Administrator\Dashboard\BlankPanel')
@inject('databasePanel', 'App\Http\Terranet\Administrator\Dashboard\DatabasePanel')
@inject('membersPanel', 'App\Http\Terranet\Administrator\Dashboard\MembersPanel')

@extends($template->layout())

@section('scaffold.content')
<div class="row-fluid">
    <div class="col-lg-12 col-md-12 col-sm-12">
        {!! $blankPanel->render() !!}
    </div>
</div>

<div class="row-fluid">
    <div class="col-lg-6 col-md-12 col-sm-12">
        {!! $membersPanel->render() !!}
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12">
        {!! $databasePanel->render() !!}
    </div>
</div>
@endsection
