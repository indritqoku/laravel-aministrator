<div class="user-panel">
    <div class="pull-left image">
        <img src="{{ asset($config->get('assets_path') . '/img/admin.png') }}" class="img-circle" alt="{{ auth()->user()->name }}">
    </div>
    <div class="pull-left info">
        <p>{{ auth()->user()->name }}</p>
        <i class="fa fa-circle text-success"></i> Online
    </div>
</div>

<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" id="q" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
            <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>

{!! $navigation->render('sidebar', '\Terranet\Administrator\Navigation\Presenters\Bootstrap\SidebarMenuPresenter') !!}

@section('scaffold.js')
    <script>
        // implement search feature
    </script>
@append
