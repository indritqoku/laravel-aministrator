<?php

namespace Terranet\Administrator\Services;

use Terranet\Administrator;
use Terranet\Administrator\Contracts\Module;
use Terranet\Administrator\Contracts\Services\Finder as FinderContract;
use Terranet\Administrator\Filters\Assembler;

class Finder implements FinderContract
{
    /**
     * @var Module
     */
    protected $module;

    /**
     * @var
     */
    protected $model;

    /**
     * Query assembler
     *
     * @var
     */
    protected $assembler;

    public function __construct(Module $module)
    {
        $this->module = $module;
        $this->model = $module->model();
    }

    /**
     * Fetch all items from repository
     *
     * @return mixed
     */
    public function fetchAll()
    {
        return $this->getQuery()->paginate($this->perPage());
    }

    /**
     * Find a record by id or fail
     *
     * @param       $key
     * @param array $columns
     * @return mixed
     */
    public function find($key, $columns = ['*'])
    {
        $this->model = $this->model->newQueryWithoutScopes()->findOrFail($key, $columns);

        return $this->model;
    }

    protected function perPage()
    {
        return method_exists($this->module, "perPage")
            ? $this->module->perPage()
            : 20;
    }

    /**
     * Build Scaffolding Index page query
     *
     * @return mixed
     */
    public function getQuery()
    {
        $this->assembler();

        $this->handleFilter();

        $this->handleSortable();

        return $this->assembler()->getQuery();
    }

    protected function handleFilter()
    {
        if ($filter = app('scaffold.filter')) {
            if ($filters = $filter->filters()) {
                $this->assembler()->filters($filters);
            }

            if ($magnet = app('scaffold.magnet')) {
                $this->handleMagnetFilter($magnet);
            }

            if ($scopes = $filter->scopes()) {
                if (($scope = $filter->scope()) && ($found = $scopes->get($scope))) {
                    $this->assembler()->scopes(
                        $found
                    );
                }
            }
        }
    }

    /**
     * Auto-scope fetching items to magnet parameter
     * @param MagnetParams $magnet
     */
    protected function handleMagnetFilter(MagnetParams $magnet)
    {
        $magnetFilters = array_build($magnet->toArray(), function ($key) {
            return [$key, [
                'type' => 'text',
                'label' => $key
            ]];
        });

        $magnetFilters = new Administrator\Filter(app('request'), $magnetFilters, []);

        $this->assembler()->filters($magnetFilters->filters());
    }

    /**
     * Extend query with Order By Statement
     */
    protected function handleSortable()
    {
        $sortable = app("scaffold.sortable");
        $element = $sortable->element();
        $direction = $sortable->direction();

        if ($element && $direction) {
            if (is_string($element)) {
                $this->assembler()->sort($element, $direction);
            }
        }
    }

    /**
     * Get the query assembler object
     *
     * @return Assembler
     */
    protected function assembler()
    {
        if (null === $this->assembler) {
            $this->assembler = (new Assembler($this->model));
        }

        return $this->assembler;
    }
}
