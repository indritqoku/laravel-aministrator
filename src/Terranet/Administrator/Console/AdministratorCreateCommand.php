<?php

namespace Terranet\Administrator\Console;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Contracts\Hashing\Hasher;

class AdministratorCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'administrator:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new administrator user.';

    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * Create a new command instance.
     *
     * @param Hasher $hasher
     */
    public function __construct(Hasher $hasher)
    {
        parent::__construct();
        $this->hasher = $hasher;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask("Name", 'Administrator');
        $email = $this->ask("Email", 'admin@example.com');
        $password = $this->ask("Password", 'secret');

        $data = $this->prepareData($name, $email, $password);

        $this->tryUpdatingRole($this->createUser($data));
    }

    /**
     * @param $name
     * @param $email
     * @param $password
     * @return array
     */
    protected function prepareData($name, $email, $password)
    {
        $data = compact('name', 'email', 'password');

        $data['password'] = $this->hasher->make($data['password']);

        return $data;
    }

    /**
     * Try to update user's role to admin if column 'role' exists
     *
     * @param User $user
     * @return bool
     */
    protected function tryUpdatingRole(User $user)
    {
        try {
            $user->role = 'admin';
            return $user->save();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $data
     * @return User
     */
    protected function createUser($data)
    {
        try {
            return (new User)->create($this->dataWithId($data));
        } catch (\Exception $e) {
            return (new User)->create($data);
        }
    }

    /**
     * @param $data
     * @return array
     */
    protected function dataWithId($data)
    {
        return array_merge(['id' => 1], $data);
    }
}
