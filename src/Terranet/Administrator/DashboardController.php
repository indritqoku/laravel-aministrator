<?php

namespace Terranet\Administrator;

use App\Http\Controllers\Controller as BaseController;
use Terranet\Administrator\Middleware\Authenticate;
use Terranet\Administrator\Middleware\AuthProvider;
use Terranet\Administrator\Middleware\Resources;

class DashboardController extends BaseController
{
    public function __construct()
    {
        $this->middleware(Resources::class);
        $this->middleware(AuthProvider::class);
        $this->middleware(Authenticate::class);
    }

    public function index()
    {
        return view(app('scaffold.template')->layout('dashboard'));
    }
}
