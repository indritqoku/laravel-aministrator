<?php

namespace Terranet\Administrator\Form;

use Terranet\Administrator\Contracts\Form\Element as ElementInterface;
use Terranet\Administrator\Contracts\Form\FormElement;
use Terranet\Administrator\Contracts\Form\Relationship;
use Terranet\Administrator\Contracts\Form\Validable;
use Terranet\Administrator\Traits\Form\FormControl;
use Terranet\Administrator\Traits\Form\HasRelation;
use Terranet\Administrator\Traits\Form\ValidatesFormElement;

abstract class Element implements FormElement, ElementInterface, Validable, Relationship
{
    use FormControl, ValidatesFormElement, HasRelation;

    protected $translatable = false;

    public function __construct($name)
    {
        // by default set label equal to name
        $this->setLabel($this->setName($name));
    }

    public function initFromArray(array $options = null)
    {
        $this->attributes = array_merge($this->attributes, array_except($options, 'type'));

        $this->validateAttributes();

        $this->decoupleOptionsFromAttributes();

        $this->setDefaultValue();

        return $this;
    }

    protected function setDefaultValue()
    {
        if (is_callable($this->getValue()) && ($closure = $this->getValue())) {
            $this->setValue(
                call_user_func($closure, $this->getRepository())
            );
        } elseif ($this->hasRelation() && ($repository = $this->getRepository())) {
            $this->setValue(
                $this->extractValueFromEloquentRelation($repository)
            );
        } elseif ($magnet = $this->isMagnetParameter()) {
            $this->setValue(
                $magnet[$this->getName()]
            );
        }
    }

    final public function html()
    {
        $this->setDefaultValue();

        return $this->renderInput() . $this->renderErrors();
    }

    /**
     * Each subclass should have this method realized
     *
     * @return mixed
     */
    abstract public function renderInput();

    protected function getRepository()
    {
        return app('scaffold.model') ?: app('scaffold.module')->model();
    }

    protected function isMagnetParameter()
    {
        return array_key_exists(
            $this->getName(),
            $magnet = app('scaffold.magnet')->toArray()
        ) ? $magnet : false;
    }
}
