<?php

namespace Terranet\Administrator;

use Codesleeve\LaravelStapler\Providers\L5ServiceProvider as StaplerServiceProvider;
use DaveJamesMiller\Breadcrumbs\Facade as BreadcrumbsFacade;
use DaveJamesMiller\Breadcrumbs\ServiceProvider as BreadcrumbsServiceProvider;
use Illuminate\Html\FormFacade;
use Illuminate\Html\HtmlFacade;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Pingpong\Menus\MenuFacade;
use Pingpong\Menus\MenusServiceProvider;
use Terranet\Administrator\Providers\ArtisanServiceProvider;
use Terranet\Administrator\Providers\ContainersServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        $baseDir = base_path('vendor/terranet/administrator');

        /**
         * Publish & Load routes
         */
        $packageRoutes = "{$baseDir}/publishes/routes.php";
        $publishedRoutes = app_path("Http/Terranet/Administrator/routes.php");
        $this->publishes([$packageRoutes => $publishedRoutes]);
        if (! $this->app->routesAreCached()) {
            $routesFile = file_exists($publishedRoutes) ? $publishedRoutes : $packageRoutes;

            /** @noinspection PhpIncludeInspection */
            require_once $routesFile;
        }

        /**
         * Publish & Load configuration
         */
        $this->publishes(["{$baseDir}/publishes/config.php" => config_path('administrator.php')], 'config');
        $this->mergeConfigFrom("{$baseDir}/publishes/config.php", 'administrator');

        /**
         * Publish & Load views, assets
         */
        $this->publishes(["{$baseDir}/publishes/public" => public_path('administrator')], 'public');
        $this->publishes(["{$baseDir}/publishes/views" => base_path('resources/views/vendor/administrator')], 'views');
        $this->loadViewsFrom("{$baseDir}/publishes/views", 'administrator');

        /**
         * Publish & Load translations
         */
        $this->publishes(
            ["{$baseDir}/publishes/translations" => base_path('resources/lang/vendor/administrator')],
            'translations'
        );
        $this->loadTranslationsFrom("{$baseDir}/publishes/translations", 'administrator');

        /**
         * Publish default Administrator Starter Kit: modules, dashboard panels, policies, etc...
         */
        $this->publishes(
            ["{$baseDir}/publishes/Modules" => app_path("Http/Terranet/Administrator/Modules")],
            'boilerplate'
        );
        $this->publishes(
            ["{$baseDir}/publishes/Dashboard" => app_path("Http/Terranet/Administrator/Dashboard")],
            'boilerplate'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $dependencies = [
            ArtisanServiceProvider::class,
            ContainersServiceProvider::class,
            BreadcrumbsServiceProvider::class => [
                'Breadcrumbs' => BreadcrumbsFacade::class,
            ],
            HtmlServiceProvider::class => [
                'Html' => HtmlFacade::class,
                'Form' => FormFacade::class,
            ],
            StaplerServiceProvider::class,
            MenusServiceProvider::class => [
                'AdminNav' => MenuFacade::class,
            ],
        ];

        array_walk($dependencies, function ($package, $provider) {
            if (is_string($package) && is_numeric($provider)) {
                $provider = $package;
                $package = null;
            }

            if (! $this->app->getProvider($provider)) {
                $this->app->register($provider);

                if (is_array($package)) {
                    foreach ($package as $alias => $facade) {
                        class_alias($facade, $alias);
                    }
                }
            }
        });
    }
}
