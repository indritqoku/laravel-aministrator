<?php

namespace Terranet\Administrator\Traits\Module;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Doctrine\DBAL\Schema\Column;
use Terranet\Translatable\Translatable;

trait HasForm
{
    /**
     * Provides array of editable columns
     *
     * @return array
     */
    public function form()
    {
        return $this->scaffoldForm();
    }

    /**
     * Build editable columns based on table columns metadata
     *
     * @return array
     */
    protected function scaffoldForm()
    {
        $model = $this->model();

        $editable = array_merge(
            [],
            $translatable = $this->scaffoldTranslatable($model),
            $model->getFillable()
        );

        return array_build($editable, function ($key, $column) use ($model, $translatable) {
            $type = $this->inputType($column, $model);

            if (is_string($type)) {
                $type = ['type' => $type];
            }

            if (in_array($column, $translatable)) {
                $type['translatable'] = true;
            }

            if ($model instanceof StaplerableInterface && array_key_exists($column, $model->getAttachedFiles())) {
                $type['type'] = count($model->$column->styles) > 1 ? 'image' : 'file';
            }

            return [$column, $type];
        });
    }

    /**
     * @param $column
     * @param $eloquent
     * @return string
     */
    protected function inputType($column, $eloquent)
    {
        if (method_exists($this, 'inputTypes') && ($types = $this->inputTypes())) {
            if (array_key_exists($column, $types)) {
                return $types[$column];
            }
        }

        // detect attachments
        if ($eloquent instanceof StaplerableInterface && ($attachments = $eloquent->getAttachedFiles())) {
            if ($this->isAttachment($column, $attachments)) {
                return $this->getAttachmentType($column, $attachments);
            }
        }

        $columns = $this->allColumns($eloquent);

        // map column database type to type
        if (array_key_exists($column, $columns)) {
            $column = $columns[$column];

            return $this->mapColumnTypeToFieldType($column);
        }

        return 'text';
    }

    protected function mapColumnTypeToFieldType(Column $column)
    {
        switch ($this->columnType($column)) {
            case 'TextType':
                return 'textarea';

            case 'BooleanType':
                return 'bool';

            case 'IntegerType':
            case 'DecimalType':
            case 'FloatType':
                return 'number';

            case 'DateTimeType':
                return 'datetime';

            case 'DateType':
                return 'date';
        }

        return 'text';
    }

    /**
     * @param $column
     * @return string
     */
    protected function columnType(Column $column)
    {
        return class_basename($column->getType());
    }

    /**
     * @param $column
     * @param $attachments
     * @return bool
     */
    protected function isAttachment($column, $attachments)
    {
        return array_key_exists($column, $attachments);
    }

    /**
     * @param $column
     * @param $attachments
     * @return array
     */
    protected function getAttachmentType($column, $attachments)
    {
        $type = count($attachments[$column]->getConfig()->styles) <= 1 ? 'file' : 'image';

        return ['type' => $type];
    }

    protected function scaffoldTranslatable($model)
    {
        if ($model instanceof Translatable) {
            return $model->getTranslatedAttributes();
        }

        return [];
    }

    /**
     * @param $eloquent
     * @return array
     */
    protected function allColumns($eloquent)
    {
        static $columns = null;

        if (null === $columns) {
            $columns = app('scaffold.schema')->columns($eloquent->getTable());

            if ($eloquent instanceof Translatable && ($eloquent->getTranslatedAttributes())) {
                $translationModel = $eloquent->getTranslationModel();

                $columns = array_merge($columns, app('scaffold.schema')->columns($translationModel->getTable()));
            }
        }

        return $columns;
    }
}
