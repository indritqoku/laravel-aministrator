<?php

namespace Terranet\Administrator\Traits\Module;

use Input;

trait AllowFormats
{
    /**
     * Available export formats
     *
     * @return array
     */
    public function formats()
    {
        return $this->scaffoldFormats();
    }

    protected function scaffoldFormats()
    {
        return ['xml', 'csv', 'json'];
    }

    /**
     * Get exportable url
     *
     * @param $format
     * @return string
     */
    public function makeExportableUrl($format)
    {
        $payload = array_merge([
            'module' => $this->url(),
            'format' => $format,
        ], Input::all());

        return route('scaffold.export', $payload);
    }
}
