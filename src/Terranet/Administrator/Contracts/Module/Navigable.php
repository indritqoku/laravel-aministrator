<?php

namespace Terranet\Administrator\Contracts\Module;

interface Navigable
{
    const MENU_SIDEBAR = "sidebar";

    const MENU_TOOLS = "tools";

    /**
     * Navigation container which Resource belongs to
     * Available: sidebar, tools
     *
     * @return mixed
     */
    public function navigableIn();

    /**
     * Navigation group which Resource belongs to
     *
     * @return string
     */
    public function group();

    /**
     * Resource order number
     *
     * @return int
     */
    public function order();

    /**
     * Attributes assigned to <a> element
     *
     * @return mixed
     */
    public function linkAttributes();
}
