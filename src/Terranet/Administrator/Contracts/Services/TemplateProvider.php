<?php

namespace Terranet\Administrator\Contracts\Services;

interface TemplateProvider
{
    /**
     * Scaffold layout
     *
     * @return string
     */
    public function layout();

    /**
     * Scaffold index template
     *
     * @param  $partial
     * @return mixed array|string
     */
    public function index($partial = 'index');

    /**
     * Scaffold view templates
     *
     * @param $partial
     * @return mixed array|string
     */
    public function view($partial = 'index');

    /**
     * Scaffold edit templates
     *
     * @param $partial
     * @return mixed array|string
     */
    public function edit($partial = 'index');
}
