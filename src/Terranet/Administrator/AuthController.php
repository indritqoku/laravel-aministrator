<?php

namespace Terranet\Administrator;

use App\Http\Controllers\Controller as BaseController;
use Event;
use Illuminate\Contracts\Auth\Guard;
use Terranet\Administrator\Middleware\AuthProvider;
use Terranet\Administrator\Requests\LoginRequest;

class AuthController extends BaseController
{
    /**
     * @var Guard
     */
    protected $user;

    public function __construct(Guard $user)
    {
        $this->user = $user;

        $this->middleware(AuthProvider::class);
    }

    public function postLogin(LoginRequest $request)
    {
        $config = app('scaffold.config');

        // basic login policy
        $credentials = $request->only(
            [
                $config->get('auth_identity', 'username'),
                $config->get('auth_credential', 'password')
            ]
        );

        // extend auth policy by allowing custom login conditions
        if ($conditions = $config->get('auth_conditions', [])) {
            $credentials = array_merge($credentials, $conditions);
        }

        $remember = (int)$request->get('remember_me', 0);

        if ($this->user->attempt($credentials, $remember, true)) {
            Event::fire('admin.login', [$this->user]);

            return redirect()->to($config->get('home_page'));
        }

        return redirect()->back()->withErrors(['Login attempt failed']);
    }

    public function getLogin()
    {
        return view(app('scaffold.template')->auth('login'));
    }

    public function getLogout()
    {
        Event::fire('admin.logout', [$this->user]);

        $this->user->logout();

        return redirect()->to('admin/login');
    }
}
