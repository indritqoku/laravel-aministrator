<?php

namespace Terranet\Administrator\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Pingpong\Menus\Menu;
use Pingpong\Menus\MenuBuilder;
use Terranet\Administrator\Actions;
use Terranet\Administrator\Contracts\Module;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Traits\ResolvesClasses;

class Resources
{
    use ResolvesClasses;

    /**
     * @var Application
     */
    protected $application;

    /**
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }


    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        foreach ($this->collectModules() as $fileInfo) {
            $this->resolveClass($fileInfo, function ($module) {
                if ($this->accessGranted($module)) {
                    if ($module instanceof Module) {
                        $this->registerModule($module);
                    }

                    if ($module instanceof Navigable
                        && ($navigation = $this->application->make('scaffold.navigation'))) {
                        $this->makeNavigation($module, $navigation);
                    }
                }
            });
        }

        return $next($request);
    }

    protected function collectModules()
    {
        return $this->application['files']->allFiles(
            app_path($this->application['scaffold.config']->get('path.module'))
        );
    }

    /**
     * @param $module
     * @param $navigation
     * @param $group
     * @return mixed
     */
    protected function findOrCreateGroup(Module $module, MenuBuilder $navigation, $group)
    {
        if (! $sub = $navigation->whereTitle($group)) {
            $sub = $navigation->dropdown($group, function () {
                //
            }, 99, ['id' => $module->url(), 'icon' => 'fa fa-folder']);
        }

        return $sub;
    }

    /**
     * @param $module
     * @param $navigation
     * @return mixed
     */
    protected function validateContainer(Module $module, Menu $navigation)
    {
        $container = $module->navigableIn();

        if (! array_key_exists($container, $navigation->all())) {
            $message = "Can not add \"{$module->title()}\" to \"{$container}\" menu. Available menus: " .
                        join(", ", array_keys($navigation->all())) . ".";
            throw new \InvalidArgumentException($message);
        }

        return $container;
    }

    /**
     * Check module access
     *
     * @param $module
     * @return bool
     */
    protected function accessGranted(Module $module)
    {
        $handler = $module->actions();
        $handler = new $handler($module);
        $gate = new Actions($handler, $module);

        return $gate->authorize("index", $module->model());
    }

    /**
     * @param Module $module
     * @param $navigation
     */
    protected function makeNavigation(Module $module, $navigation)
    {
        // Force ordering menus
        $navigation = $navigation->instance($this->validateContainer($module, $navigation));

        $order = 1;
        if ($group = $module->group()) {
            $navigation = $this->findOrCreateGroup($module, $navigation, $group);

            $order = $module->order() ? : count($navigation->getChilds()) + 1;
        }

        $navigation->route('scaffold.index', $module->title(), ['module' => $module->url()], $order, $module->linkAttributes());
    }

    /**
     * @param $module
     */
    protected function registerModule(Module $module)
    {
        $this->application->instance("scaffold.module.{$module->url()}", $module);
    }
}
