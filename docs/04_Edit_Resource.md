# Form

TerranetAdmin gives you complete control over the output of the form by creating a thin DSL on top of Illuminate\Form package:

Any $fillable column will be available for editing by default.
Note: If your model implements Translatable interface, $translatedAttributes will be available too, so you don't need to define them manually.

Also you can extend default form by adding columns:

```php
public function form()
{
	return array_merge($this->scaffoldForm(), [
		'column1' => ['type' => 'text'],
		'column2' => ['type' => 'tinymce', 'translatable' => true]
	]);
}
```

Note: checkout src/helpers.php file for any available form helpers.

## Partials

If you want to split a custom form into a separate partial check Customization > Templates section:

## Manage Files, Images

By default TerranetAdmin uses ```codesleeve/laravel-stapler``` package for files/images management. So by default any $fillable files and images columns will be automatically wrapped as ```File``` element with ability to delete/re-upload existent file.

For more info please checkout its documentation by accessing https://github.com/CodeSleeve/laravel-stapler.
