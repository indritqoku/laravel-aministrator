# TerranetAdmin

TerranetAdmin is a framework for creating administration interfaces.
It abstracts common business application patterns to make it simple for developers to implement beautiful and elegant interfaces with very little effort.


# Installation

There are 2 ways to install TerranetAdmin: via gitlab repository (only for private members) and by downloading zip archive.

## Via Gitlab repository (Private members)

Add a new repository to your composer.json

```json
"repositories": [
  ...
  {
    "type": "git",
    "url": "git@gitlab.top.md:terranet/administrator.git"
  }
  ...
]
```

## Via Zip archive (Public way)

Downlad the [latest](#) version of TerranetAdmin and extract this archive to your project ```packages``` directory.

Enter to ```projects/administrator``` directory and run:
  
```sh
git init;
git add .
git commit -m 'First init'
```

Add a new repository to your composer.json

```json
"repositories": [
  ...
  {
    "type": "git",
    "url": "./packages/administrator"
  }
  ...
]
```

## Install package

```sh
composer require terranet/administrator
```

After the package installed, add a new service provider to the providers array in config/app.php file.

```php
'providers' => [
    ...
    Terranet\Administrator\ServiceProvider::class
]
```

Then, publish package's assets by running: ```php artisan vendor:publish``` OR ```php artisan vendor:publish --provider=Terranet\\Administrator\\ServiceProvider``` to publish only administrator's files.

Optionally you can run ```php artisan administrator:create``` to create new administrator user.

# Configuration

Check out configuration file: `config/administrator.php` for basic administrator options.

To start using administrator package you need to define authorization rules for users who should have access to backend.

# Getting Started

Open user model (Default: App\User) (modify ```auth_model``` section to use another model) and add a method isSuperAdmin() which will be used do determine if current logged is Super Admin.

You may restrict users to enter backend panel by rewriting `permission` configuration section.

Also you are able to define credential columns used for authentication as like as additional authentication conditions specific for your project, like "role='admin'" or "active=1", etc...