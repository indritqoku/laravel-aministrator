# Dashboard

Checkout 'administrator::layouts/dashboard.blade.php' if you need to add or edit/delete dashboard panels.

To create new Dashboard panel run:

```sh
php artisan administrator:panel <name>
```


# Templates

If you need to customize how index or form is rendered overwrite default Template class: ```php artisan administrator:template Users``` following naming convention or referencing from Resource like:

```php
protected $template = MyUserTemplate::class
```

# Breadcrumbs

To build breadcrumbs TerranetAdmin uses https://github.com/davejamesmiller/laravel-breadcrumbs package. Please checkout its documentation if you have any questions.

You are free to change the way how breadcrumbs are rendered by replacing default Breadcrumbs instance by your own:

```php
protected $breadcrumbs = MyBreadcrumbs::class;
```

# Settings

In order to support settings within TerranetAdmin interface you'll need to install the [terranet/options](https://gitlab.top.md/terranet/options) package.

Note: After installing it be sure the Terranet\Options\ServiceProvider is connected before the Terranet\Administrator\ServiceProvider in config/app.php.

To allow options in TerranetAdmin just run:

```sh
php artisan administrator:resource:settings
```

# Localizer

In order to allow building multilingual sites you'll need a [terranet/localizer](https://gitlab.top.md/terranet/localizer) package.

Note: After installing it be sure the Terranet\Localizer\ServiceProvider is connected before the Terranet\Administrator\ServiceProvider in config/app.php.

```sh
php artisan administrator:resource:languages
```