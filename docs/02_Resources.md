# Resources

`Note!` *Every TerranetAdmin resource corresponds to an Eloquent model. So before creating a resource you must first create an Eloquent model for it.*

## Create a Resource

The basic command for creating a resource is:

```sh
php artisan administrator:resource <name> <model>
```

The generator will produce an empty resource in  app/Http/Terranet/Administrator/Modules directory.

## Rename the Resource

By default, any references to the resource (menu, routes, buttons, etc) in the interface will use the name of the class. You can rename the resource by using the title() method.

```php
public function title()
{
  return "Article";
}
```

To change the resource url define method url() like so:
```php
public function url()
{
  return "articles";
}
```

The resource will then be available at /admin/articles.

## Columns

Any $fillable, indexed and $dates columns will be available for listing by default.

Adding new columns is simple, just let the TerranetAdmin to know what to show and how, if needed.

Note: .dot notation also works while referencing the relationship columns;

```php
public function columns()
{
    return array_merge($this->scaffoldColumns(), [
        'Contacts' => [
            'elements' => [
                'user_contacts.phone',
                'user_contacts.skype',
                'user_contacts.icq'                
            ]
        ],
        'Posts' => ['output' => function ($row) {
            if ($posts = $row->posts->lists('title', 'id')) {
                return admin\output\ul($posts, function($post) {
                    return link_to(route('scaffold.index', ['module' => 'posts']), "Posts");
                });
            }
        }]
    ]);    
}
```

## Actions on a Resource

All CRUD actions are enabled by default. These can be enabled, disabled or even extended for a given resource just creating an action class:

There are few options to create an Action:

* Create an action class with the same name as your resource.
Let's say you have Resource called Users, so just create an action Users

```sh
php artisan administrator:action Users
```

* Create a new action

```sh
php artisan administrator::action MyUserAction
```

and reference to from Users Resource:

```php
protected $actions = MyUserAction::class;
```

### Callback actions and Url actions

There are 2 different types of actions available: callback and url.

### External url action (marked as @action link):

```php
	/**
     * Example of custom url-action
     *
     * @action link
     * @param $eloquent
     * @return bool
     */
    public function posts(Post $eloquent)
    {
        return link_to(route('scaffold.index', [
        	'module'  => 'posts',
            'user_id' => $eloquent->id
        ]), "Posts");
    }
```

To enable or disable this action in function of user permissions just define method with prefix ```can``` like so:

```php
public function canPosts(User $user, Post $post)
```

which should return ```true``` current user is authorized to perform this action or ```false``` if not.

### Callback action (marked as @action callback):

```php
	/**
     * Example of custom callback-action
     *
     * @action callback
     * @param $eloquent
     * @return bool
     */
    public function activate(User $user)
    {
        return $user->fill(['active' => 1])->save();
    }
```

and authorization method:

```php
	/**
     * Define the case when an user can be activated
     */
    public function canActivate(User $admin, User $user)
    {
        return $admin->isSuperAdmin() && $user->isLocked();
    }
```

### CRUD actions authorization

Just define ```can``` methods for any action you want to check, like so:

```php
public function canEdit(User $admin, User $user)
{
	return true or false;
}

public function canDelete(User $admin, User $user)
{
	return true or false
}
```

## Batch Actions

By default TerranetAdmin provides one very useful batch action: ```removeSelected```.

You are free to add your batch actions as like other single action, just marking it with ```@global``` flag within docblock comment.

```php
/**
 * Remove selected collection
 *
 * @param       $eloquent
 * @param array $collection
 * @global
 * @return $this
 */
public function removeSelected($eloquent, array $collection = [])
{
	return $eloquent->newQueryWithoutScopes()
	->whereIn('id', $collection)
	->get()
	->each(function ($item) {
		return $this->authorize('delete', $item)
			? $this->delete($item)
			: $item;
	});
}
```

# Resource Finder service

If you need to completely replace or update the record retrieving code (e.g., you have a custom complex query with table joins, complex aggregation methods, etc...), override the resource finder. Naming convention is the same as for actions.
Just create new finder with the same name as Resource

```php
php artisan administrator:finder Users
```

or point your Resource to custom finder

```php
protected $finder = MyUserFinder::class;
```

then update or extend default finder methods by your own needs. You are free to use any Eloquent features, like eager loading, scoping, etc...
For instance, a common way to increase page performance is to elimate N+1 queries by eager loading associations:

```php
reutrn $query->with(['comments', 'users'=> function($subQuery) {
	return $subQuery->select(['id', 'name', 'email'])->whereActive(1);
}]);
```

# Resource Saver service

If default saving process does not satisfy your needs, you are free to extend or rewrite it completely:

```php
php artisan administrator:saver Users
```