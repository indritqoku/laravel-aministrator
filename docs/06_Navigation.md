# Navigation

To build menus, TerranetAdmin uses https://github.com/pingpong-labs/menus.

Any resource that implements Navigable contract will be displayed in the global navigation by default. To disable the resource from being displayed in the global navigation just don't implement that interface.

To customize how resource will appear in the navigation checkout the Terranet\Administrator\Contracts\Module\Navigable interface.

To change default navigation structure, checkout ```menu``` section in the ```config/administrator.php```.