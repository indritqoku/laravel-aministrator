# Index Page

Filtering and listing resources is one of the most important tasks for administering a web application. TerranetAdmin provides default tools for you to build a compelling interface into your data for the admin staff.

All index pages support scopes, filters, pagination, exports, per-item and global actions, etc...

## Filters

By default the index screen includes a "Filters" section with a filter for each searchable attribute of the registered model.

** searchable* - any indexed varchar, boolean, date or datetime column (including translated varchar columns)


You can customize the filters that are displayed as well as the type of widgets they use.

To customize filters for resource rewrite ```filters()``` method of main Resource.

```php
public function filters()
{
	// by adding filter
	$this->addFilter('name', 'text');

	// or by modifying scaffolded filters
	$this->filters = array_merge($this->scaffoldFilters(), [
		'name' => ['type' => 'text'],
		'year' => ['type' => 'select', 'options' => [
			2010 => 2010,
			2015 => 2015,
			2020 => 2020
		]]
	]);

	return $this->filters;
}
```

Supported filters: ```text```, ```select```, ```date```, ```daterange```.

If a filter require more control while fetching resource you can define an optional \Closure $query attribute.

```php
$this->addFilter('user', 'text', 'Owner', [], function ($query) {
	return $query->whereActive(1);
});
```

To disable filters for a specific resource just return an empty array:

```php
public function filters()
{
	return [];
}
```

## Scopes

You can define custom scopes for your index page. This will add a tab bar above the index table to quickly filter your collection on pre-defined scopes. Terranet Admin will parse your resource model for any available scope.

In addition, if your model implements SoftDeletes contract some useful scopes (like withTrashed, onlyTrashed) will be available too.

To disable a scope just add it to Resource $hiddenScopes array or add @hidden docblock flag to the scope method;

Also you are able to define your custom, non-model scopes, like so:

```php
public function scopes()
{
	$this->scaffoldScopes();

	$this->addScope("Managers", function($query) {
		return $query->whereId(2);
	});

	return $this->scopes;
}
```

To rename model-based scope, just define it like so:

```php
$this->addScope("Admins", "Admin");
```

Where the 1st argument is a label and 2nd argument is model scope name.

## Sorting

TerranetAdmin is smart enough to parse your model table for any potentialy "sortable" columns. Any indexed column is "sortable" by definition...

To disable sorting by any column add this column to "unSortable" array in your Resource class:

```php
protected $unSortable = ['id'];
```

To enable sorting by other columns, define "sortables" like so:

```php
public function sortable()
{
	return [
		'id', 'email', 'name'
	];
}
```

To enable more complex "sortabe" column (for ex. by column from joined table) just define how to sort:

```php
public function sortable()
{
	return [
		'id', 'email', 'name',
		'phone' => function ($query, $element, $direction) {
			return $query->join('user_contacts', function ($join) {
				$join->on('users.id', '=', 'user_contacts.user_id');
			})->orderBy("users_contacts.{$element}", $direction);
		}
	];
}
```

TerranetAdmin even supports QueryBuilder class name notation:

```php
public function sortable()
{
	return [
		'id', 'email', 'name',
		'skype' => SortBySkype::class
	];
}
```
Where SortBySkype is a simple class which implements Terranet\Administrator\Contract\QueryBuilder interface with single method ```build```:

```php
namespace App\Http\Terranet\Administrator\Queries;

use Terranet\Administrator\Contracts\QueryBuilder;

class SortBySkype implements QueryBuilder
{
    private $query;

    private $element;

    private $direction;

    public function __construct($query, $element, $direction)
    {
        $this->query = $query;
        $this->element = $element;
        $this->direction = $direction;
    }

    public function build()
    {
        return $this->query->join('user_contacts', function ($join) {
            $join->on('users.id', '=', 'user_contacts.user_id');
        })->orderBy("users_contacts.{$this->element}", $this->direction);
    }
}
```

## Pagination

You can set the number of records per page per resources:
Default pagination perPage value is 20.
if you want to change this value just rewrite ```perPage``` method in your Resource class.

```php
public function perPage()
{
	return 10;
}
```

## Download Links (Export)

You can easily remove or customize the download links you want displayed:

```php
public function formats()
{
	return ['xml','csv'];
}
```

By default xml, csv, json formats are supported.

If you want to support other formats just define them:

In resource class:

```php
public function formats()
{
	// preserve default formats
	$formats = $this->scaffoldFormats();

	return array_push($formats, ['pdf']);
}
```

Note: you have to actually implement PDF rendering for your action, TerranetAdmin does not provide this feature.
This setting just allows you to specify formats that you want to show up under the index collection.

So define an export method in your actions handler class:

```php
public function toPdf($query)
{
	$pdf = 'code that exports collection';

	return response($pdf, 200, ['Content-Type' => 'application/pdf']);
}
```

Probably you'll need to use a 3rd party PDF rendering library to get the PDF generation you want.