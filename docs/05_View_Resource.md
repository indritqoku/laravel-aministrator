# Show Resource Page

The show block is rendered within the context of the view and uses Html syntax.

Note: By default any Eloquent relationship marked as @widget within the docblock comment will be added as a panel to view screen.

for example if your Post model has ```user``` relationship:

```php
/**
 * @widget
 */
public function user()
{
	return $this->belongsTo(User::class);
}
```

this relationship will be automatically rendered as a sidebar block.

You can add as many widgets as you like by defining them in Resource class:

```php
public function widgets()
{
	return array_merge($this->scaffoldWidgets(), [
		'widget1' => view('some.useful.widget'),
		'widget2' => new AnyClassImplementsWidgetableContract
	]);
}
```
